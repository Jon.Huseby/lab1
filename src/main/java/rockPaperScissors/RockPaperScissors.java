package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true){
            System.out.println("Let's play round " + roundCounter);

            String hChoice = humanChoice();
            String cChoice = computerChoice();
        
            String isWinner = isWinner(hChoice,cChoice);
            System.out.println("Human chose " + hChoice + ", computer chose " + cChoice + ". " + isWinner);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            Boolean playMore = playMore();
        
            if (playMore) {
                roundCounter+=1;
                continue;
            } else {
                System.out.println("Bye bye :)");
                break;
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String humanChoice(){
        while (true){
            String hChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            String result = hChoice.toLowerCase();
            boolean checkAns = rpsChoices.contains(result);
            if (checkAns)
                return result;
            else
                System.out.println("I do not understand " + hChoice + ". Could you try again?");
                continue;
        }
    }

    public String computerChoice(){
        Random rand = new Random();
        String cChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return cChoice;
    }

    public String isWinner(String h, String c){
        String winner;
        String r = "rock";
        String p = "paper";
        String s = "scissors";
        if (c.equals(h)){
            winner = "It's a tie!";
            return winner;
        } else if (c.equals(r) && h.equals(s)){
            winner = "Computer wins!";
            computerScore+=1;
            return winner;
        } else if (c.equals(s) && h.equals(p)){
            winner = "Computer wins!";
            computerScore+=1;
            return winner;
        } else if (c.equals(p) && h.equals(r)){
            winner = "Computer wins!";
            computerScore+=1;
            return winner;
        } else {
            winner = "Human wins!";
            humanScore+=1;
            return winner;
        }
        
    }

    public Boolean playMore(){
        while (true){
            String yesNo = readInput("Do you wish to continue playing? (y/n)?");
            if (yesNo.equals("y")){
                return true;
            } else if (yesNo.equals("n")){
                return false;
            } else {
                System.out.println("I do not understand " + yesNo + ". Could you try again?");
                continue;
            }
        }
    }
}